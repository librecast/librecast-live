	const registerComponent = function(nodeName, component) {
		console.log(`registering ${nodeName} component`);
		components.push({ 'nodeName': nodeName, 'component': component });
	}

	const render = function() {
		const tree = document.createTreeWalker(document.getRootNode(), NodeFilter.SHOW_ELEMENT);
		for (let node = tree.currentNode; node; node = tree.nextNode()) {
			const q = [];
			components.filter(c => c.nodeName === node.nodeName)
			.forEach(c => {
				console.log('rendering ' + c.nodeName);
				const component = new c.component();
				component.node = node;
				q.push(component);
			});
			while (q.length > 0) {
                                const c = q.shift();
                                c.render();
                        };
		}
	}

	const main = function() {
		store = new Store();
		chat = new Chat(lctx);

		registerComponent('APP', App);
		registerComponent('AVATAR', Avatar);
		registerComponent('CHANNELLIST', ChannelList);
		registerComponent('CHANNELLISTITEM', ChannelListItem);
		registerComponent('CHATBAR', ChatBar);
		registerComponent('CHATBOX', ChatBox);
		registerComponent('CHATPANE', ChatPane);
		registerComponent('CHATMSGS', ChatMsgs);
		registerComponent('CHATTOPIC', ChatTopic);
		registerComponent('CLOSEX', CloseX);
		registerComponent('FOOTBOX', FootBox);
		registerComponent('HEADER', Header);
		registerComponent('LOGINMENU', LoginMenu);
		registerComponent('LOGOBOX', LogoBox);
		registerComponent('MENUBOX', MenuBox);
		registerComponent('MIDDLE', Middle);
		registerComponent('PROFILE', Profile);
		registerComponent('PROJECTBOX', ProjectBox);
		registerComponent('SETPASSWORD', SetPassword);
		registerComponent('SIGNUPBOX', SignupBox);
		registerComponent('STATS', Stats);
		registerComponent('USERLIST', UserList);
		registerComponent('VIDBOX', VidBox);
		registerComponent('VIDCHAT', VidChat);

		// load state
		fetch('/data/0001.state.json').then(response => {
			if (response.ok) {
				return response.json();
			}
		})
		.then(newstate => {
			store.mutate("activeChannel", newstate.activeChannel);
			store.mutate("chat", newstate.chat);
		});
		const token = window.location.pathname.split("/")[2];
		if (token !== undefined) {
                        store.mutate('passToken', token);
                        store.mutate('showSignup', "confirmSignup");
                }
		render();
	}
