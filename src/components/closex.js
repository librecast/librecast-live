class CloseX extends Component {
	nodeName = 'CLOSEX';
	constructor() {
		super();
	}
	close() {
		console.log("closeX clicked");
		store.mutate("closex", true);
	}
	get template() {
		return `<div class="closex" click="close">X</div>`;
	}
}
