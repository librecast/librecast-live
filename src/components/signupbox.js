class SignupBox extends Dialog {
	nodeName = 'SIGNUPBOX';
	forms = {
		signup: `
			<h1>Join Librecast LIVE!</h1>
			<div class="tabmenu">
				<button click='showLogin'>Log In</button>
				<button click='showSignup' class="down">Sign Up</button>
			</div>
			<div class="dialogform">
				<form action="javascript:void(0);">
					<label for="emailAddress">Email Address</label>
					<input id="emailAddress" type="email" placeholder="you@example.com" />
					<button type="submit" click="signup">Sign Up</button>
				</form>
			</div>`
		,
		login:  `
			<h1>Sign In to Librecast LIVE!</h1>
			<div class="tabmenu">
				<button click='showLogin' class="down">Log In</button>
				<button click='showSignup'>Sign Up</button>
			</div>
			<div class="dialogform">
				<form action="javascript:void(0);">
					<label for="emailAddress">Email Address</label>
					<input id="emailAddress" type="email" placeholder="you@example.com" />
					<label for="password">Password</label>
					<input id="password" type="password" />
					<button type="submit" click="login">Log In</button>
				</form>
			</div>`
		,
		confirmSignup: `
			<h1>Complete Account Signup</h1>
			<div class="dialogform">
				<form action="javascript:void(0);">
					<label for="password">Password</label>
					<input id="password" type="password" />
					<button type="submit" click="setPassword">Sign Up</button>
				</form>
			</div>`
		,
		loggingIn: `
			<h1>Logging In</h1>
			<p>Please wait a moment...</p>`
		,
		signedup: `
			<h1>Sign Up Successful</h1>
			<p>You will receive an email with instructions to complete your signup.</p>
		`
	}
	constructor() {
		super();
		this.content = this.forms[store.state.showSignup];
	}
	close() {
		store.mutate('showSignup', undefined);
	}
	async login() {
		const emailAddress = document.getElementById("emailAddress");
	        const password = document.getElementById("password");
		store.mutate('showSignup', "loggingIn");
		const s1 = Symbol('timeout')
		const timeout = new Promise((resolve, reject) => setTimeout(() => resolve(s1), 2000))
		try {
			const login = live.login(emailAddress, password)
			const res = await Promise.race([login, timeout])
			if (res === s1) {
				console.error('login TIMEOUT')
				store.mutate('showSignup', "login");
				alert("Server Error: login timeout")
			} else {
				console.log("logged in successfully");
				this.close();
				store.mutate('loggedIn', true);
				if (store.getItem('nick') === undefined) {
					store.mutate('showProfile', 'profileGeneral');
				}
				else {
					store.mutate('showChat', true);
				}

			}
		}
		catch(e) {
			console.error("login failed")
			store.mutate('showSignup', "login");
			alert("login failed")
		}
	}
	showLogin() {
		store.mutate('showSignup', "login");
	}
	showSignup() {
		store.mutate('showSignup', "signup");
	}
	setPassword() {
		console.log("setting password");
		const password = document.getElementById("password");
		live.setPassword(store.state.passToken, password)
		.then(() => {
			alert("password set");
			store.mutate('showSignup', "login");
		})
		.catch(() => {
			console.error("failed to set password");
			alert("failed to set password");
		});
	}
	signup() {
		live.signup().then(() => {
		})
		.catch(() => {
			console.error("signup failed");
			alert("signup failed");
		});
		store.mutate('showSignup', "signedup");
	}
}
