class Middle extends Component {
	nodeName = 'MIDDLE';
	constructor() {
		super();
		store.subscribe('state.showSignup', this.render);
		store.subscribe('state.showProfile', this.render);
		store.subscribe('state.showChat', this.render);
		store.subscribe('state.loggedIn', this.render);
	}
	get template() {
		let html = `<div class="middle">`;
		if (store.state.showSignup !== undefined) {
			html += `<signupbox></signupbox>`;
		}
		if (store.state.loggedIn !== undefined && store.state.showChat !== undefined) {
			html += `<chatbox></chatbox>`;
		}
		if (store.state.loggedIn !== undefined && store.state.showProfile !== undefined) {
			html += `<profile></profile>`;
		}
		html += `</div>`;
		return html;
	}
}
