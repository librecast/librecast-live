class ChannelListItem extends Component {
	nodeName = 'CHANNELLISTITEM';
	constructor() {
		super();
	}
	switchChannel(e) {
		const chan = e.target.attributes.channel.value;
		store.mutate("activeChannel", chan);
	}
	get template() {
		const channel = this.props.channel;
		let classes = 'channellistitem';
		if (store.state.activeChannel === channel) {
			classes += " active";
		}
		if (chat.channels[channel] !== undefined) {
			classes += ' ready';
		}
		return `<li class="${classes}" click="switchChannel" channel="${channel}">${channel}</li>`;
	}
	componentDidMount = (() => {
		store.subscribe(`state.chat.channels.${this.props.channel}.status`, this.render);
	});
}
