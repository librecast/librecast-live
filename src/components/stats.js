class Stats extends Component {
	nodeName = 'STATS';
	constructor() {
		super();
		store.subscribe('state.chat', this.render);
	}
	userSelect(e) {
		const user = e.target.attributes.user.value;
	}
	get template() {
		const chan = store.state.chat.channels[store.state.activeChannel];
		const bytin = (chan.bytin !== undefined) ? chan.bytin : 0;
		const bytout = (chan.bytout !== undefined) ? chan.bytout : 0;
		let html = `<div class="stats">
			<h2>Statistics</h2>
			<dl>
			<div>
				<dt>bytes received</dt><dd>${bytin}</dd>
			</div>
			<div>
				<dt>bytes sent</dt><dd>${bytout}</dd>
			</div>
			</dl>

		</div>`;
		return html;
	}
}
