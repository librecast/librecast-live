class TopBar extends Component {
	nodeName = 'TOPBAR';
	constructor() {
		super();
		store.subscribe('state.title', this.render);
	}
	myfunc = () => {
		store.mutate('title', "Was Clicked");
	}
	get template() {
		return `<div class="topbar" click="myfunc">
			${store.state.title}
		</div>`;
	}
}
