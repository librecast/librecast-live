class UserList extends Component {
	nodeName = 'USERLIST';
	constructor() {
		super();
		store.subscribe('state.chat', this.render);
	}
	userSelect(e) {
		const user = e.target.attributes.user.value;
	}
	get template() {
		let html = `<div class="userlist">
			<h2>Users</h2>
			<ul>`;
		const chan = store.state.chat.channels[store.state.activeChannel];
		chan.users.forEach(user => {
                        html += `<li click="userSelect" user="${user.nick}"><avatar></avatar>${user.nick}</li>`;
                });
		html += `</ul></div>`;
		return html;
	}
}
