class Dialog extends Component {
	nodeName = 'DIALOG';
	constructor() {
		super();
		store.subscribe('state.closex', this.close);
	}
	close() {
		console.log("closex clicked");
	}
	get template() {
		return `<div class="dialog">
			<closex></closex>
			${this.content}
		</div>`;
	}
}
