class LogoBox extends Component {
	nodeName = 'LOGOBOX';
	constructor() {
		super();
	}
	get template() {
		return `<div class="logobox">
			<img class="logo" src="/media/live.svg" height="100" width="100" alt="" />
		</div>`;
	}
}
