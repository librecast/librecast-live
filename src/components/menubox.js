class MenuBox extends Component {
	nodeName = 'MENUBOX';
	constructor() {
		super();
	}
	get template() {
		return `<div class="menubox">
			<nav>
				<ul>
					<!--
					<li><a href="/about.html">About</a></li>
					<li><a href="/code.html">Code</a></li>
					<li><a href="/roadmap.html">Roadmap</a></li>
					<li><a href="/team.html">Team</a></li>
					<li><a href="/videos.html">Videos</a></li>
					-->
				</ul>
			</nav>
		</div>`;
	}
}
