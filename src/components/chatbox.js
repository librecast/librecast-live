class ChatBox extends Component {
	nodeName = 'CHATBOX';
	constructor() {
		super();
		store.subscribe('state.activeChannel', this.render);
		store.subscribe('state.showStats', this.render);
	}
	close() {
		store.mutate('showChat', undefined);
	}
	get template() {
		let html = `<div class="chatbox">
			<chattopic></chattopic>
			<section>
				<channellist></channellist>
				<chatpane></chatpane>
				<!--userlist></userlist-->`;
		if (store.state.showStats) {
			html += `<stats></stats>`;
		}
		html += `</section>
		</div>`;
		return html;
	}
}
