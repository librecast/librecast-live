class Profile extends Dialog {
	nodeName = 'PROFILE';
	constructor() {
		super();
		this.nick = store.getItem('nick');
		this.bio = store.getItem('bio');
		if (this.nick === undefined) { this.nick = ''; }
		if (this.bio === undefined) { this.bio = ''; }
		this.forms = {
			profileGeneral: `
				<h1>Profile</h1>
				<div class="tabmenu">
					<button click='showLogin' class="down">General</button>
					<button click='showSignup'>Particular</button>
				</div>
				<div class="dialogform">
					<form action="javascript:void(0);">
						<label for="nick">Nick</label>
						<input id="nick" type="text" placeholder="your user nick/handle" value="${this.nick}" />
						<label for="bio">Bio</label>
						<input id="bio" type="text" placeholder="bio" value="${this.bio}" />
						<button type="submit" click="save">Save Changes</button>
					</form>
				</div>`
		}
		this.content = this.forms[store.state.showProfile];
	}
	close() {
		store.mutate('showProfile', undefined);
		if (store.state.nick !== undefined) {
			store.mutate('showChat', true);
		}
	}
	saveField(fieldName) {
		const value = document.getElementById(fieldName).value;
		store.mutate(fieldName, value, true);
	}
	save() {
		// TODO store transactions to batch updates?
		//TODO store.begin();
		this.saveField("nick");
		this.saveField("bio");
		this.close();
		//TODO store.commit();
		//TODO store.abort();
	}
}
