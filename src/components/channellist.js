class ChannelList extends Component {
	nodeName = 'CHANNELLIST';
	constructor() {
		super();
	}
	get template() {
		let html = `<div class="channellist">
			<h2>Channels</h2>
			<ul>`;
		//store.state.chat.channels.forEach(chan => {
		for (const [key, value] of Object.entries(store.state.chat.channels)) {
			html += `<channellistitem channel="${key}"></channellistitem>`
		};
		html += `</ul></div>`;
		return html;
	}
}
