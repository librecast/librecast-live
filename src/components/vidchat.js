class VidChat extends Component {
	nodeName = 'VIDCHAT';
	constructor() {
		super();
	}
	close() {
		store.mutate('showChat', undefined);
	}
	get template() {
		let html = `<div class="chat vidchat">
			<chattopic></chattopic>
			<section>
				<vidbox></vidbox>
				<vidbox></vidbox>
				<chatpane></chatpane>
			</section>
		</div>`;
		return html;
	}
}
