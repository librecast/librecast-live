class Header extends Component {
	nodeName = 'HEADER';
	constructor() {
		super();
	}
	get template() {
		return `<div class="header">
			<logobox></logobox>
			<projectbox></projectbox>
			<loginmenu></loginmenu>
		</div>`;
	}
}
