class LoginMenu extends Component {
	nodeName = 'LOGINMENU';
	connectButtons = `
		<button class="login" click='showLogin' title="Log In"></button>
	`;
	logoutButtons = `
		<button class="stats" click='stats' title="Stats"></button>
		<button class="logout" click='logout' title="Log Out"></button>
		<button class="profile" click='showProfile' title="Edit Profile"></button>
	`;
	constructor() {
		super();
		store.subscribe('state.loggedIn', this.render);
	}
	logout() {
		console.log("logging out");
		store.mutate('loggedIn', undefined);
	}
	stats() {
		store.mutate('showStats', !store.state.showStats);
	}
	showDialog(mode) {
		store.mutate('showSignup', mode);
	}
	showLogin() {
		this.showDialog('login');
	}
	showProfile() {
		store.mutate('showProfile', 'profileGeneral');
	}
	showSignup() {
		this.showDialog('signup');
	}
	get template() {
		this.content = (store.state.loggedIn) ?  this.logoutButtons : this.connectButtons;
		return `<div class="loginmenu">
			${this.content}
		</div>`;
	}
}
