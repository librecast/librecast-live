class App extends Component {
	nodeName = 'APP';
	constructor() {
		super();
	}
	get template() {
		return `<div class="outerspace">
				<header></header>
				<middle></middle>
				<footbox></footbox>
			</div>
		`
	}
}
