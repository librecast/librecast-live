class ProjectBox extends Component {
	nodeName = 'PROJECTBOX';
	constructor() {
		super();
	}
	get template() {
		return `<div class="projectbox">
				<div class="projectname">
					Libre<span class="latter">cast</span>
					<span class="itsalive">LIVE</span>
                        	</div>
                        </div>
		</div>`;
	}
}
