class FootBox extends Component {
	nodeName = 'FOOTBOX';
	get template() {
		return `<div class="footbox">
		<p class="invisible">&nbsp;</p><!-- clear footer in text browsers -->
		</div>`;
	}
}
