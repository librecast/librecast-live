class ChatBar extends Component {
	nodeName = 'CHATBAR';
	constructor() {
		super();
	}
	clear() {
		this.node.firstElementChild.value = "";
		this.node.firstElementChild.focus();
	}
	keypress(e) {
		if (e.keyCode === 13) {
			this.newMsg();
			this.clear();
		}
	}
	newMsg() {
		const str = he.encode(this.node.firstElementChild.value);
		const msgText = `<p>${str}</p>`;
		chat.send(store.state.activeChannel, msgText);
	}
	get template() {
		let html = `<div class="chatbar">
			<input type="text" keypress="keypress" />
		</div>`;
		return html;
	}
}
