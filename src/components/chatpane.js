class ChatPane extends Component {
	nodeName = 'CHATPANE';
	constructor() {
		super();
		store.subscribe('state.activeChannel', this.render);
	}
	get template() {
		console.log(store.state.activeChannel);
		let html = `<div class="chatpane">
			<h2>#${store.state.activeChannel}</h2>
			<chatmsgs></chatmsgs>
			<chatbar></chatbar>
		</div>`;
		return html;
	}
}
