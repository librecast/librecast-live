class ChatMsgs extends Component {
	nodeName = 'CHATMSGS';
	constructor() {
		super();
		store.subscribe('state.chat', this.render);
	}
	componentDidMount = () => {
		this.node.scrollTop = this.node.scrollHeight;
	}
	get template() {
		let html = `<div class="chatmsgs">`;
		if (store.state.activeChannel !== undefined) {
			const chan = store.state.chat.channels[store.state.activeChannel];
			const opt = {
				weekday: 'short', year: 'numeric', month: 'short', day: 'numeric',
				hour: 'numeric', minute: 'numeric', second: 'numeric',
				hour12: false,
			};
			chan.msgs.forEach(msg => {
				const sent = new Intl.DateTimeFormat('default', opt).format(msg.timestamp);
				html += `<div class="chatmsg">`;
				html += `<div class="msghdr">`;
				html += `<avatar src="/media/${msg.username}.jpg"></avatar>`;
				html += `<div class="username">${msg.username}</div>`;
				html += `<div class="timestamp">${sent}</div>`;
				html += `</div>`;
				html += `<section>${msg.msg}</section>`;
				html += `</div>`;
			});
		}
		html += `</div>`;
		return html;
	}
}
