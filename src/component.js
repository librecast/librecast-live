class Component {
  componentDidMount = () => {
  }

  render = () => {
    const range = document.createRange()
    const frag = range.createContextualFragment(this.template)
    const sevents = []

    // add all events we allow in components
    sevents.push('click')
    sevents.push('keypress')

    for (let child = frag.firstChild; child; child = child.nextElementSibling) {
      // component event handlers
      let tree = document.createTreeWalker(child, NodeFilter.SHOW_ELEMENT)
      for (let node = tree.currentNode; node; node = tree.nextNode()) {
        sevents.forEach(e => {
          if (node.attributes[e]) {
            node.addEventListener(e, (evt) => {
              this[node.attributes[e].value](evt)
            })
          }
        })
      };

      // search fragment for more components before inserting
      const q = []
      tree = document.createTreeWalker(child, NodeFilter.SHOW_ELEMENT)
      for (let node = tree.currentNode; node; node = tree.nextNode()) {
        components.filter(c => c.nodeName === node.nodeName)
          .forEach(c => {
            const component = new c.component()
            component.node = node
            component.parent = this
            component.props = [];
            [...node.attributes].forEach(a => {
              component.props[a.name] = a.value
            })
            q.push(component)
          })
      }
      while (q.length > 0) {
        const c = q.shift()
        console.log('rendering component: ' + c.nodeName)
        c.render()
      };
    }
    const newnode = frag.firstChild
    this.node.replaceWith(...frag.children)
    this.node = newnode
    this.componentDidMount()
  }
}
