const authComboKeyHex = "fbdd352740551bd867f1970e7fc1a8fb23b0f84865beb7550aa51e5aa349e9271fd320c9db88bdbf249527b9720d90b30c03bdf0e06efe667e3e1e7e6f243c1f";

const login = function(emailAddress, password) {
	const kp = sodium.crypto_box_keypair();
	let loggedin = false;
	return new Promise((resolve, reject) => {
		const auth = new Auth(lctx, authComboKeyHex, kp, (opcode, flags, fields, pre) => {
			if (loggedin) return;
			loggedin = true;
			const view = new DataView(pre)
			const responseCode = view.getUint8(0).toString();
			if (responseCode === "0") {
				resolve();
			}
			else {
				reject();
			}
		});
		auth.ready.then(() => {
			auth.login(emailAddress.value, password.value);
		});
	});
}

const setPassword = function(token, password) {
	const kp = sodium.crypto_box_keypair();
	let passet = false;
	return new Promise((resolve, reject) => {
		const auth = new Auth(lctx, authComboKeyHex, kp, (opcode, flags, fields, pre) => {
			if (passet) return;
			passet = true;
			const view = new DataView(pre)
			const responseCode = view.getUint8(0).toString();
			if (responseCode === "0") {
				resolve();
			}
			else {
				reject();
			}
		})
		auth.ready.then(() => {
			auth.setPassword(token, password.value);
		});
	});
}

const signup = function() {
	const emailAddress = document.getElementById("emailAddress");
	const kp = sodium.crypto_box_keypair();
	let signup = false;
	return new Promise((resolve, reject) => {
		const auth = new Auth(lctx, authComboKeyHex, kp, (opcode, flags, fields, pre) => {
			if (signup) return;
			signup = true;
			const view = new DataView(pre)
			const responseCode = view.getUint8(0).toString();
			if (responseCode === "0") {
				resolve();
			}
			else {
				reject();
			}
		})
		auth.ready.then(() => {
			auth.signup(emailAddress.value, null);
		});
	});
}
