// eslint-disable-next-line no-unused-vars
class Store {
  _state = {}
  event = {}

  get state () {
    return this._state
  }

  set state (newstate) {
    this._state = newstate
  }

  getItem = (key) => {
    if (this._state[key] === undefined && window.localStorage.getItem(key) !== null) {
      this._state[key] = window.localStorage.getItem(key)
    }
    return this._state[key]
  }

  mutate = (key, value, cacheLocal) => {
    console.log(`mutating ${key}`)
    this._state[key] = value
    if (cacheLocal === true) {
      window.localStorage.setItem(key, value)
    }
    this.publish('state.' + key)
  }

  publish = (event) => {
    console.log(`publish ${event}`)
    if (this.event[event] instanceof Event) {
      document.dispatchEvent(this.event[event])
    }
  }

  subscribe = (event, listener) => {
    console.log(`subscribing to ${event}`)
    if (this.event[event] === undefined) {
      console.log('new event created: ' + event)
      this.event[event] = new Event(event)
    }
    document.addEventListener(event, listener)
  }

  unsubscribe = (event, listener) => {
    console.log(`unsubscribing from ${event}`)
    document.removeEventListener(event, listener)
  }
}
