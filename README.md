# Librecast LIVE!

<img src="https://librecast.net/media/live.svg" height="150" width="150"
alt="Librecast LIVE logo"/>

## Live Streaming Video Platform with IPv6 Multicast

## Status

Work in Progress.

This is the source code for the public demo, which so far demonstrates:

* multicast authentication (signup + confirm + login flow)
* multicast chat

We have a lot more to add, so watch this space.

## Dependencies

Both are bundled with this program in lib/:
- librecast.js (https://codeberg.org/librecast/librecast.js.git)
- libsodium.js (https://github.com/jedisct1/libsodium.js/)

## Dev Dependencies
- qunit

## License

GPL2 or, at your option GPL3
