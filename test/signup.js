QUnit.module('authentication', function() {
	QUnit.test('new user account - bad email address', function(assert) {
		console.log("== TEST: new user account - bad email address");
		assert.timeout(2000);
		const done = assert.async(1);
		const kp = sodium.crypto_box_keypair();
		const localpart = sodium.to_hex(sodium.randombytes_buf(16));
		const invalidEmail = "@live.librecast.net";
		const password = sodium.to_hex(sodium.randombytes_buf(16));
		util.auth_init(kp, function (op, flags, fields, pre) {
			const view = new DataView(pre);
			const responseCode = view.getUint8(0).toString();
			assert.ok(op === 0x1, "opcode=" + op);
			assert.ok(responseCode === "1", "signup rejected");
			done();
		})
		.then(function () {
			util.signup(kp, invalidEmail, password);
		});
	});

	QUnit.test('use bad token', function(assert) {
		console.log("== TEST: set password - bad token");
		assert.timeout(2000);
		const done = assert.async(1);
		const kp = sodium.crypto_box_keypair();
		const token = sodium.to_hex(sodium.randombytes_buf(16));
		const password = sodium.to_hex(sodium.randombytes_buf(16));
		util.auth_init(kp, function (op, flags, fields, pre) {
			const view = new DataView(pre);
			const responseCode = view.getUint8(0).toString();
			assert.ok(op === 0x4, "opcode=" + op);
			assert.ok(responseCode === "1", "token rejected");
			done();
		})
		.then(function () {
			util.setPassword(kp, token, password);
		});
	});

	QUnit.test('bad login', function(assert) {
		console.log("== TEST: login - bad login");
		assert.timeout(2000);
		const done = assert.async(1);
		const kp = sodium.crypto_box_keypair();
		const localpart = sodium.to_hex(sodium.randombytes_buf(16));
		const email = localpart + "@live.librecast.net";
		const password = sodium.to_hex(sodium.randombytes_buf(16));
		util.auth_init(kp, function (op, flags, fields, pre) {
			const view = new DataView(pre);
			const responseCode = view.getUint8(0).toString();
			assert.ok(op === 0x8, "opcode=" + op);
			assert.ok(responseCode === "1", "login failed");
			done();
		})
		.then(function () {
			util.login(kp, email, password);
		});
	});

	QUnit.test('new user account', function(assert) {
		console.log("== TEST: new user account");
		assert.timeout(3000);
		const done = assert.async(3);
		const kp = sodium.crypto_box_keypair();
		console.log(kp);
		const localpart = sodium.to_hex(sodium.randombytes_buf(32));
		const email = localpart + "@live.librecast.net";
		const password = sodium.to_hex(sodium.randombytes_buf(16));
		const seed = kp.publicKey.slice(0, sodium.randombytes_SEEDBYTES);
		const token = sodium.to_hex(sodium.randombytes_buf_deterministic(sodium.crypto_box_PUBLICKEYBYTES, seed));
		const hextoken = sodium.to_hex(token);

		util.auth_init(kp, function (op, flags, fields, pre) {
			const view = new DataView(pre)
			const responseCode = view.getUint8(0).toString();
			switch (op) {
				case 0x1:
					assert.ok(responseCode === "0", "signup confirmed");
					util.setPassword(kp, token, password);
					done();
					break;
				case 0x4:
					assert.ok(responseCode === "0", "token used, password set");
					util.login(kp, email, password)
					done();
					break;
				case 0x8:
					assert.ok(responseCode === "0", "login successful");
					const capClear =util.authCheckSignature(fields[0]);
					const capFields = util.wireUnpack7Bit(capClear.buffer, 8);
					assert.ok(capFields.length === 3, "token has 3 fields");

					/* TODO: check expires (pre 8 bytes of token) */
					//view.getUint8(pre);
					assert.ok(util.keysEqual(capFields[0], kp.publicKey), "token key matches")
					assert.strictEqual(sodium.to_string(capFields[1]), "service", "service matches");
					/* TODO: checking userid requires config.testmode and AUTH_TESTMODE */
					//assert.strictEqual(capFields[2], userid, "userid matches");

					util.register(kp, fields[0]);
					done();
					break;
				default:
					throw "unknown opcode " + op + " received";
			}
		}, 1)
		.then(function () {
			util.signup(kp, email, password);
		});

		/* TODO: do something with cap token */
	});
});
