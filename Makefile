.PHONY: all src dist

all: src dist

src dist:
	$(MAKE) -C $@
